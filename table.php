<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <html>
    <head>

        <!-- Website Title & Description for Search Engine purposes -->
        <title>Ozarus.com</title>
        <meta name="description" content="Learn how to code your first responsive website with the new Twitter Bootstrap 3.">

        <!-- Mobile viewport optimized -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="includes/css/bootstrap-glyphicons.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="includes/css/styles.css">

        <!-- Include Modernizr in the head, before any other Javascript -->
        <script src="includes/js/modernizr-2.6.2.min.js"></script>




    </head>
<body>

<div class="container" id="main">

    <div class="navbar navbar-fixed-top">
        <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <button class="navbar-toggle" data-target=".navbar-responsive-collapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id = "about"><img src="images/logo.png" alt="Your Logo"></a>
            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="http://localhost/PHPProject/HomePage.html">Home</a>

                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <strong class="caret"></strong></a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">Web Design</a>
                            </li>

                            <li>
                                <a href="#">Web Development</a>
                            </li>

                            <li>
                                <a href="#">SEO</a>
                            </li>

                            <li class="divider"></li>

                            <li class="dropdown-header">More Services</li>

                            <li>
                                <a href="#">Content Creation</a>
                            </li>

                            <li>
                                <a href="#">Social Media Marketing</a>
                            </li>
                        </ul><!-- end dropdown-menu -->
                    </li>

                </ul>
                <form class="navbar-form pull-left">
                    <input type="text" class="form-control" placeholder="Search this site..." id="searchInput">
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </form><!-- end navbar-form -->

                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Abhishek <strong class="caret"></strong></a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><span class="glyphicon glyphicon-wrench"></span> Settings</a>
                            </li>

                            <li>
                                <a href="#"><span class="glyphicon glyphicon-refresh"></span> Update Profile</a>
                            </li>

                            <li>
                                <a href="#"><span class="glyphicon glyphicon-briefcase"></span> Billing</a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="../PHPProject/logout.php"><span class="glyphicon glyphicon-off"></span> Sign out</a>
                            </li>
                        </ul>
                    </li>
                </ul><!-- end nav pull-right -->
            </div><!-- end nav-collapse -->

        </div><!-- end container -->
    </div><!-- end navbar -->


    <!-- First try for the online version of jQuery-->
    <script src="http://code.jquery.com/jquery.js"></script>

    <!-- If no online access, fallback to our hardcoded version of jQuery -->
    <script>window.jQuery || document.write('<script src="includes/js/jquery-1.8.2.min.js"><\/script>')</script>

    <!-- Bootstrap JS -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- Custom JS -->
    <script src="scripts/script.js"></script>
    <script src="scripts/scroll.js"></script>

    <br>
    <br>
    <br>
   <br>

    <br>
    <br>
    <br>
    <br>

    <br>
    <br>
    <br>
    <br>

    <?php
/**
 * Created by PhpStorm.
 * User: Abhishek
 * Date: 12/12/2015
 * Time: 1:50 AM
 */


$dbConnect = array(
    'server' => 'localhost',
    'user' => 'root',
    'pass' => '',
    'name' => 'test_db'
);

$db = new mysqli(
    $dbConnect['server'],
    $dbConnect['user'],
    $dbConnect['pass'],
    $dbConnect['name']
);


if ($db->connect_errno>0) {
    echo "Database connection error" . $db->connect_error;
    exit;
}

$sql = "SELECT * FROM `test_db` ORDER BY `name`";

$result = $db->query($sql);


?>
<h1>TABLE DISPLAY</h1>
<table border="1" cellpadding="5" cellspacing="0">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Password</th>
        <th>Secrets</th>
        <th>Delete</th>
    </tr>

    <?php

    while ($row = $result->fetch_object()) {
        $id = $row->id;
        $name = htmlentities($row->name, ENT_QUOTES, "UTF-8");
        $password = htmlentities($row->password, ENT_QUOTES, "UTF-8");
        $secrets = htmlentities($row->secrets, ENT_QUOTES, "UTF-8");
        ?>

        <tr>
            <td><?php echo $id; ?></td>
            <td><?php echo $name; ?></td>
            <td><?php echo $password; ?></td>
            <td><?php echo $secrets; ?></td>
            <td><a href = "delete.php?id=<?php echo $id?>">Delete</a></td>
        </tr>

        <?php

    }

    ?>

</table>



    <h1>TABLE CREATION INSERTION</h1>



    <form action="insert.php" method="post">
        <table border="1" cellpadding="5" cellspacing="0">
            <tr style="text-align: left;">
                <th style="width: 150px;">Name</th>
                <th style="width: 150px;">Password</th>
                <th style="width: 200px;">Secrets</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td style="width: 150px;"><input type="text" name="username"></td>
                <td style="width: 150px;"><input type="text" name="password"></td>
                <td style="width: 200px;"><input type="text" name="secrets"></td>
                <td><input type="submit" value="Submit"></td>
            </tr>
        </table>
    </form>