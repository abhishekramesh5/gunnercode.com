<?php
/**
 * Created by PhpStorm.
 * User: Abhishek
 * Date: 12/12/2015
 * Time: 2:49 AM
 */


include 'connect.php';

if (isset($_GET['id'])) {    // GET instead of POST for value in the URL
    $id = $_GET['id'];       // only id is needed - delete other variable assignments
}


$stmt = $db->prepare("DELETE FROM `test_db` WHERE `id` = ?");   // DELETE ... FROM ... WHERE
$stmt->bind_param('i', $id);    // 'i' for integer; only parameter is $id
$stmt->execute();               // as before
$stmt->close();                 // as before


header("Location:table.php");
?>